import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CASOS {

	@Test
	void testPublic	() {
		assertEquals("PERICO QUE VUELA PERICO A LA CAZUELA", resolt.comprobarguanyador(null,4,4,3,1));
		assertEquals("A VEURE ELS PENALS...", resolt.comprobarguanyador(null,3,3,3,3));
		assertEquals("A VEURE SI HI TROBO UNA COMPETICIÓ DE SINCRONITZADA...", resolt.comprobarguanyador(null,0,1,0,1));
	}
	@Test
	void testPrivat	() {
		assertEquals("A VEURE ELS PENALS...", resolt.comprobarguanyador(null,0,0,0,0));
		assertEquals("A VEURE ELS PENALS...", resolt.comprobarguanyador(null,10,10,10,10));
		assertEquals("PERICO QUE VUELA PERICO A LA CAZUELA", resolt.comprobarguanyador(null,5,0,5,0));
		assertEquals("A VEURE SI HI TROBO UNA COMPETICIÓ DE SINCRONITZADA...", resolt.comprobarguanyador(null,3,4,0,0));
	}

}
